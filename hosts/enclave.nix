{...}: {
  imports = [
    ../profiles/libvirt_guest.nix
    ../profiles/ldap
    ../profiles/keycloak
    ../profiles/krb/server.nix
    ../profiles/pki/server.nix
    ../profiles/vaultwarden.nix
    ../profiles/sonic.nix
    ../profiles/netdata/child.nix
  ];

  networking.hostName = "enclave";

  services.netdata.apiKey = "8e5db52a-8c9b-403d-813c-08ba3e0e1899";

  security.acme.acceptTerms = true;
  security.acme.defaults.email = "sysmans+enclave@tardisproject.uk";

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/d0d9be24-c0c5-4df4-b8c9-1898c34facfe";
    fsType = "ext4";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/841B-A47C";
    fsType = "vfat";
  };

  networking.useDHCP = true;
  networking.firewall.allowedTCPPorts = [22];
}

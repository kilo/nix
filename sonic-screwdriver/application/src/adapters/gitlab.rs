use anyhow::Result;
use chrono::{Duration, Utc};
use jsonwebtoken::{Algorithm, EncodingKey, Header};
use reqwest::Client;
use serde::{Deserialize, Serialize};
use sonic_types::gitlab::GitlabPage;

use crate::config::Gitlab as GitlabConfig;

pub struct GitlabAdapter {
    conf: GitlabConfig,
    client: Client,
}

impl GitlabAdapter {
    pub fn new(conf: &GitlabConfig) -> Self {
        Self {
            client: Client::new(),
            conf: conf.clone(),
        }
    }

    /// Get all usernames of active users on gitlab
    pub async fn get_usernames(&self) -> Result<Vec<String>> {
        let mut page = 0;
        let mut usernames = vec![];
        loop {
            let resp: Vec<UserAPIRow> = self
                .client
                .get(format!(
                    "{}/api/v4/users?exclude_internal=true&active=true&page={}",
                    self.conf.url, page
                ))
                .bearer_auth(&self.conf.api_key)
                .send()
                .await?
                .json()
                .await?;

            if resp.is_empty() {
                break;
            }

            usernames.extend(resp.into_iter().map(|r| r.username));
            page += 1;
        }

        Ok(usernames)
    }

    /// Get gitlab pages endpoints for a given domain.
    /// This uses an internal API, which is pretty bad, however there's no other way unfortunately.
    pub async fn get_pages(&self, domain: String) -> Result<Vec<GitlabPage>> {
        // Encode a JWT using the internal secret
        let key = EncodingKey::from_base64_secret(&self.conf.internal_secret)?;
        let exp = Utc::now() + Duration::hours(1);
        let jwt = jsonwebtoken::encode(
            &Header::new(Algorithm::HS256),
            &JWTClaims {
                iss: "gitlab-pages",
                iat: Utc::now().timestamp(),
                exp: exp.timestamp(),
            },
            &key,
        )?;

        let resp: InternalPagesResp = self
            .client
            .get(format!(
                "{}/api/v4/internal/pages?host={}",
                self.conf.url, domain,
            ))
            .header("Gitlab-Pages-Api-Request", jwt)
            .send()
            .await?
            .json()
            .await?;

        Ok(resp
            .lookup_paths
            .into_iter()
            .map(|p| GitlabPage {
                domain: domain.clone(),
                prefix: p.prefix,
            })
            .collect())
    }
}

#[derive(Deserialize)]
struct UserAPIRow {
    username: String,
}

#[derive(Serialize)]
struct JWTClaims {
    iss: &'static str,
    iat: i64,
    exp: i64,
}

#[derive(Deserialize)]
struct InternalPagesResp {
    lookup_paths: Vec<LookupPath>,
}

#[derive(Deserialize)]
struct LookupPath {
    prefix: String,
}

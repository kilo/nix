use std::{error::Error, fmt::Display};

#[derive(Debug)]
pub enum DBError {
    NotFound,
    ConstraintFailed,
    OtherError(sqlx::Error),
}
impl From<sqlx::Error> for DBError {
    fn from(value: sqlx::Error) -> Self {
        match value {
            sqlx::Error::RowNotFound => DBError::NotFound,
            sqlx::Error::Database(db) if db.code().map(|c| c == "1555").unwrap_or(false) => {
                DBError::ConstraintFailed
            }
            e => DBError::OtherError(e),
        }
    }
}

impl Error for DBError {}
impl Display for DBError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            DBError::NotFound => write!(f, "Could not find entry in database"),
            DBError::ConstraintFailed => write!(f, "A constraint failed to pass"),
            DBError::OtherError(e) => write!(f, "{}", e),
        }
    }
}

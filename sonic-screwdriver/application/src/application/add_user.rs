use anyhow::{Context, Result};
use sonic_types::{Password, User, Username};

use crate::App;

impl App {
    pub async fn add_user(&self, user: &User) -> Result<Password> {
        let _attrs = self
            .ldap
            .add_user(user)
            .await
            .context("Error adding user to ldap")?;

        let password = Password::gen_random();

        self.krb
            .add_principal(user.username(), &password)
            .await
            .context("Error adding user to kerberos")?;

        Ok(password)
    }

    pub async fn is_username_taken(&self, username: &Username) -> Result<bool> {
        self.ldap.username_taken(username).await
    }
}

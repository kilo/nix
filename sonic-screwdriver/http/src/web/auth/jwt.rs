use std::collections::HashMap;

use axum::{
    async_trait,
    extract::FromRequestParts,
    http::{request::Parts, StatusCode},
    response::{IntoResponse, Redirect, Response},
};
use axum_extra::extract::CookieJar;
use jsonwebtoken::{decode, decode_header, jwk::Jwk, DecodingKey, Validation};

use serde::{Deserialize, Serialize};
use sonic_application::config::Config;
use sonic_types::Username;

use crate::state::AppState;

#[derive(Deserialize, Debug, Serialize, Clone, PartialEq, Eq)]
/// Represents the fields keycloak puts into our JWT token
pub struct KeycloakTokenFields {
    preferred_username: String,
    given_name: String,
    family_name: String,
    email: String,
    roles: Vec<String>,
    azp: String,
}

#[derive(Debug, Clone, Serialize)]
/// Stores user information
/// Add this to request args to require login
pub struct UserInfo {
    pub uid: Username,
    pub first_name: String,
    pub last_name: String,
    pub email: String,
    pub is_admin: bool,
}

#[async_trait]
impl FromRequestParts<AppState> for UserInfo {
    type Rejection = Redirect;

    async fn from_request_parts(
        parts: &mut Parts,
        state: &AppState,
    ) -> Result<Self, Self::Rejection> {
        let cookies = CookieJar::from_headers(&parts.headers);
        let token = match cookies.get("jwt") {
            Some(x) => x,
            None => return Err(Redirect::to("/login")),
        };

        // Validate the json web token
        let validator = &state.auth.jwks;
        let token_info = match validator.validate(token.value()) {
            Some(x) => x,
            None => return Err(Redirect::to("/login")),
        };

        let username = match token_info.preferred_username.try_into() {
            Ok(x) => x,
            Err(_) => return Err(Redirect::to("/login")),
        };

        Ok(UserInfo {
            uid: username,
            first_name: token_info.given_name,
            last_name: token_info.family_name,
            email: token_info.email,
            is_admin: token_info.roles.contains(&"admin".to_string()),
        })
    }
}

pub struct UserIsAdmin;

#[axum::async_trait]
impl FromRequestParts<AppState> for UserIsAdmin {
    type Rejection = Response;

    async fn from_request_parts(
        parts: &mut Parts,
        state: &AppState,
    ) -> Result<Self, Self::Rejection> {
        let user_info = UserInfo::from_request_parts(parts, state)
            .await
            .map_err(|r| r.into_response())?;

        if user_info.is_admin {
            Ok(UserIsAdmin)
        } else {
            Err((StatusCode::NOT_FOUND, "Not found").into_response())
        }
    }
}

pub struct JwtValidator {
    validators: HashMap<String, (Jwk, Validation)>,
    client_id: String,
}
impl JwtValidator {
    pub async fn new(config: &Config) -> anyhow::Result<Self> {
        let payload: HashMap<String, Vec<serde_json::Value>> =
            reqwest::get(&config.auth.jwk_uri).await?.json().await?;

        // This is needed to ignore key algorithms we don't recognise, since we just set that we don't actually use them for signing in the keycloak console
        let mut validators = HashMap::new();
        for k in payload.into_values().flatten() {
            if let Ok(jwk) = serde_json::from_value::<Jwk>(k) {
                let mut validation = Validation::new(jwk.common.algorithm.unwrap());
                validation.set_issuer(&[&config.auth.jwt_issuer]);
                validators.insert(jwk.common.key_id.clone().unwrap(), (jwk, validation));
            }
        }

        Ok(JwtValidator {
            validators,
            client_id: config.auth.client_id.clone(),
        })
    }
    pub fn validate(&self, token: &str) -> Option<KeycloakTokenFields> {
        let header = decode_header(token).ok()?;
        let (jwk, validation) = self.validators.get(header.kid.as_ref()?)?;
        let decoder = DecodingKey::from_jwk(jwk).unwrap();

        let token_info = decode::<KeycloakTokenFields>(token, &decoder, validation)
            .ok()?
            .claims;

        if token_info.azp != self.client_id {
            None
        } else {
            Some(token_info)
        }
    }
}

use axum::{
    extract::{Query, State},
    http::StatusCode,
    response::{IntoResponse, Redirect},
};
use axum_extra::extract::{
    cookie::{Cookie, SameSite},
    CookieJar,
};
use oauth2::{
    basic::{BasicClient, BasicRequestTokenError},
    reqwest::async_http_client,
    AuthorizationCode, CsrfToken, PkceCodeChallenge, PkceCodeVerifier, TokenResponse,
};
use serde::Deserialize;
use std::sync::Arc;

use super::{AuthState, UserInfo};

pub type OAuthClient = BasicClient;

pub async fn login(
    cookies: CookieJar,
    user: Option<UserInfo>,
    State(auth): State<Arc<AuthState>>,
) -> impl IntoResponse {
    if user.is_some() {
        return (cookies, Redirect::to("/"));
    }

    // Generate a challenge and csrf token
    let (pkce_challenge, pkce_verifier) = PkceCodeChallenge::new_random_sha256();
    let (auth_url, csrf_token) = auth
        .client
        .authorize_url(CsrfToken::new_random)
        .set_pkce_challenge(pkce_challenge)
        .url();

    // Store them so we can verify later
    let cookies = cookies
        .add(
            Cookie::new("csrf_token", csrf_token.secret().clone()), // .same_site(SameSite::Lax)
                                                                    // .finish(),
        )
        .add(
            Cookie::new("pkce_verifier", pkce_verifier.secret().clone()), // .same_site(SameSite::Lax)
                                                                          // .finish(),
        );

    // // Redirect to auth url
    (cookies, Redirect::to(auth_url.as_str()))
}

#[derive(Deserialize)]
pub struct OAuthCallback {
    state: String,
    code: String,
}

pub async fn auth(
    cookies: CookieJar,
    State(auth): State<Arc<AuthState>>,
    Query(cb): Query<OAuthCallback>,
) -> Result<impl IntoResponse, AuthCallbackError> {
    // Get challenge and csrf token verifier
    let pkce_verifier = PkceCodeVerifier::new(
        cookies
            .get("pkce_verifier")
            .ok_or(AuthCallbackError::MissingCookies)?
            .value()
            .to_string(),
    );
    let csrf_token = cookies
        .get("csrf_token")
        .ok_or(AuthCallbackError::MissingCookies)?;

    // Verify csrf token
    if csrf_token.value() != cb.state {
        return Err(AuthCallbackError::MissingCookies)?;
    }

    // Avoid using them twice
    let cookies = cookies
        .remove(Cookie::named("pkce_verifier"))
        .remove(Cookie::named("csrf_token"));

    // Do rest of token verification and exchange
    let token_result = auth
        .client
        .exchange_code(AuthorizationCode::new(cb.code))
        .set_pkce_verifier(pkce_verifier)
        .request_async(async_http_client)
        .await
        .map_err(AuthCallbackError::RequestError)?;

    // Store the tokens
    let max_age = token_result
        .expires_in()
        .ok_or(AuthCallbackError::InvalidJWT)?;
    let max_age: time::Duration = max_age.try_into().unwrap();

    let cookies = cookies.add(
        Cookie::build("jwt", token_result.access_token().secret().to_string())
            .max_age(max_age)
            .same_site(SameSite::Lax)
            .finish(),
    );

    Ok((cookies, Redirect::to("/")))
}

/// Represents an error returned by the auth callback
#[derive(Debug)]
pub enum AuthCallbackError {
    MissingCookies,
    RequestError(BasicRequestTokenError<oauth2::reqwest::Error<::reqwest::Error>>),
    InvalidJWT,
}

impl IntoResponse for AuthCallbackError {
    fn into_response(self) -> axum::response::Response {
        dbg!(self);
        (
            StatusCode::FORBIDDEN,
            "Error authenticating. Please try again.",
        )
            .into_response()
    }
}

use axum::routing::get;
use oauth2::{AuthUrl, ClientId, ClientSecret, RedirectUrl, TokenUrl};
use sonic_application::config::Config;

use crate::state::Router;

use self::{jwt::JwtValidator, oauth::OAuthClient};

mod jwt;
mod oauth;

pub use jwt::{UserInfo, UserIsAdmin};

pub struct AuthState {
    client: OAuthClient,
    jwks: JwtValidator,
}

pub async fn state(config: &Config) -> AuthState {
    // Extract oauth config
    let client = OAuthClient::new(
        ClientId::new(config.auth.client_id.clone()),
        Some(ClientSecret::new(config.auth.client_secret.clone())),
        AuthUrl::new(config.auth.auth_uri.clone()).unwrap(),
        Some(TokenUrl::new(config.auth.token_uri.clone()).unwrap()),
    )
    .set_redirect_uri(RedirectUrl::new(config.auth.redirect_uri.clone()).unwrap());
    let jwks = JwtValidator::new(config).await.unwrap();

    AuthState { client, jwks }
}

pub fn routes() -> Router {
    Router::new()
        .route("/login", get(oauth::login))
        .route("/auth", get(oauth::auth))
}

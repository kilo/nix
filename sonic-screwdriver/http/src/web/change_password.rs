use std::sync::Arc;

use axum::{
    extract::State,
    response::{IntoResponse, Redirect},
    routing::get,
    Form,
};
use axum_flash::{Flash, IncomingFlashes};
use sonic_application::App;
use sonic_types::web::change_password::UpdatePasswordRequest;

use crate::{
    context, flash::IncomingFlashesExt, state::Router, template::render, utils::ErrorResponses,
};

use super::auth::UserInfo;

pub fn routes() -> Router {
    Router::new().route("/", get(form).post(submit))
}

async fn form(user: UserInfo, flashes: IncomingFlashes) -> impl IntoResponse {
    let templ = render(
        "change_password.html",
        context! {
            user: user,
            msgs: flashes.for_template()
        },
    );

    (flashes, templ)
}

async fn submit(
    State(app): State<Arc<App>>,
    user: UserInfo,
    flash: Flash,
    Form(update): Form<UpdatePasswordRequest>,
) -> Result<(Flash, Redirect), (Flash, Redirect)> {
    if update.old_password.is_empty() || update.new_password.is_empty() {
        return Err((
            flash.error("Neither password can be empty"),
            Redirect::to("/password"),
        ));
    }

    if update.new_password != update.confirm_new_password {
        return Err((
            flash.error("New passwords do not match"),
            Redirect::to("/password"),
        ));
    }

    app.change_password(
        &user.uid,
        &update
            .old_password
            .try_into()
            .err_to_redirect(&flash, "/password")?,
        &update
            .new_password
            .try_into()
            .err_to_redirect(&flash, "/password")?,
    )
    .await
    .err_to_redirect(&flash, "/password")?;

    Ok((
        flash.success("Updated password successfuly."),
        Redirect::to("/password"),
    ))
}

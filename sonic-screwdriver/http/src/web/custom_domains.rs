use std::sync::Arc;

use crate::{
    context, flash::IncomingFlashesExt, state::Router, template::render, utils::ErrorResponses,
};
use axum::{
    extract::State,
    response::{IntoResponse, Redirect},
    routing::{get, post},
    Form,
};
use axum_flash::{Flash, IncomingFlashes};
use serde::Deserialize;
use sonic_application::App;
use sonic_types::endpoint::{AddDomain, Domain};

use super::auth::UserIsAdmin;

pub fn routes() -> Router {
    Router::new()
        .route("/custom-domains", get(list).post(create))
        .route("/custom-domains/delete", post(delete))
}

async fn list(
    State(app): State<Arc<App>>,
    flashes: IncomingFlashes,
    flash: Flash,
    _admin: UserIsAdmin,
) -> Result<impl IntoResponse, (Flash, Redirect)> {
    let mut domains = app.all_domains().await.err_to_redirect(&flash, "/")?;
    domains.retain(Domain::is_custom);

    let templ = render(
        "custom_domains.html",
        context! {
            msgs: flashes.for_template(),
            domains: domains,
        },
    );
    Ok((flash, templ))
}

#[derive(Debug, Deserialize)]
pub struct DomainSpecReq {
    domain: String,
    owner: String,
}

async fn create(
    State(app): State<Arc<App>>,
    flash: Flash,
    _admin: UserIsAdmin,
    Form(custom_domain): Form<DomainSpecReq>,
) -> Result<(Flash, Redirect), (Flash, Redirect)> {
    let custom_domain = custom_domain
        .try_into()
        .err_to_redirect(&flash, "/custom-domains")?;

    app.add_domain(&custom_domain)
        .await
        .err_to_redirect(&flash, "/custom-domains")?;

    Ok((
        flash.success("Successfully added custom domain!"),
        Redirect::to("/custom-domains"),
    ))
}

async fn delete(
    State(app): State<Arc<App>>,
    flash: Flash,
    _admin: UserIsAdmin,
    Form(custom_domain): Form<DomainSpecReq>,
) -> Result<(Flash, Redirect), (Flash, Redirect)> {
    let custom_domain = custom_domain
        .try_into()
        .err_to_redirect(&flash, "/custom-domains")?;

    app.delete_domain(&custom_domain)
        .await
        .err_to_redirect(&flash, "/custom-domains")?;

    Ok((
        flash.success("Domain deleted successfully."),
        Redirect::to("/custom-domains"),
    ))
}

impl TryInto<AddDomain> for DomainSpecReq {
    type Error = String;

    fn try_into(self) -> Result<AddDomain, Self::Error> {
        Ok(AddDomain {
            domain: self.domain,
            owner: self
                .owner
                .try_into()
                .map_err(|e| format!("Owner is not valid: {}", e))?,
        })
    }
}

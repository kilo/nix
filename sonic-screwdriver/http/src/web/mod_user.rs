use std::sync::Arc;

use axum::{
    extract::State,
    response::{IntoResponse, Redirect},
    routing::get,
    Form,
};
use axum_flash::{Flash, IncomingFlashes};
use sonic_application::App;
use sonic_types::web::mod_user::UpdateProfileRequest;

use crate::{
    context, flash::IncomingFlashesExt, state::Router, template::render, utils::ErrorResponses,
};

use super::auth::UserInfo;

pub fn routes() -> Router {
    Router::new().route("/", get(profile).post(update))
}

async fn profile(user: UserInfo, flash: IncomingFlashes) -> impl IntoResponse {
    let templ = render(
        "profile.html",
        context! {
            user: user,
            msgs: flash.for_template()
        },
    );

    (flash, templ)
}

async fn update(
    State(app): State<Arc<App>>,
    flash: Flash,
    user: UserInfo,
    update: Form<UpdateProfileRequest>,
) -> Result<(Flash, Redirect), (Flash, Redirect)> {
    app.mod_user(
        &user.uid,
        update
            .firstname
            .as_deref()
            .filter(|x| !x.is_empty() && x.len() < 32),
        update
            .lastname
            .as_deref()
            .filter(|x| !x.is_empty() && x.len() < 32),
    )
    .await
    .err_to_redirect(&flash, "/profile")?;

    Ok((
        flash.success("Successfully updated profile. This can take up to 24 hours to propagate."),
        Redirect::to("/"),
    ))
}

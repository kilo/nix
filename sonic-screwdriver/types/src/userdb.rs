//! UserDBs are databases on our communal MySQL server.
//! They belong to a single user, and each come with an application account that has full privileges on that database.
//!
//! For clarity, we use 'creating' to mean storing a record of the database in sonic's database, and 'provisioning' to mean creating the database on the server itself.

use std::ops::Deref;

use anyhow::bail;
use serde::{Deserialize, Serialize};

use super::Username;

#[derive(Debug, Deserialize, Serialize, PartialEq, Eq)]
pub struct UserDB {
    pub owner: Username,
    pub name: DBName,
}

impl UserDB {
    pub fn slug(&self) -> String {
        let mut slug = format!("{}-{}", &**self.owner, &*self.name);
        slug.truncate(30);
        slug
    }
}

#[derive(Debug, Deserialize, Serialize, Clone, PartialEq, Eq)]
pub struct DBName(String);

impl DBName {
    pub fn into_inner(self) -> String {
        self.0
    }
}

impl TryFrom<String> for DBName {
    type Error = anyhow::Error;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        if value
            .chars()
            .any(|c| !c.is_alphanumeric() && c != '_' && c != '-')
        {
            bail!("Must contain only alphanumeric characters, dashes, and underscores.")
        }

        if value.len() <= 3 || value.len() > 30 {
            bail!("Must be between 3 and 30 characters");
        }

        Ok(DBName(value))
    }
}

impl Deref for DBName {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

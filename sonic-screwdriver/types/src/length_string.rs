use std::{error::Error, fmt::Display, ops::Deref};

#[derive(Debug, Clone)]
pub struct VarLengthString<const MIN: usize, const MAX: usize>(String);

impl<const MIN: usize, const MAX: usize> VarLengthString<MIN, MAX> {
    pub fn into_inner(self) -> String {
        self.0
    }
}

impl<const MIN: usize, const MAX: usize> Deref for VarLengthString<MIN, MAX> {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<const MIN: usize, const MAX: usize> TryFrom<String> for VarLengthString<MIN, MAX> {
    type Error = StringLengthError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        if value.len() > MAX {
            Err(StringLengthError::TooHigh {
                max: MAX,
                act: value.len(),
            })
        } else if value.len() < MIN {
            Err(StringLengthError::TooLow {
                min: MIN,
                act: value.len(),
            })
        } else {
            Ok(VarLengthString(value))
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub enum StringLengthError {
    TooHigh { max: usize, act: usize },
    TooLow { min: usize, act: usize },
}

impl Display for StringLengthError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            StringLengthError::TooHigh { max, act } => {
                write!(f, "Maximum {} characters, got {}", max, act)
            }
            StringLengthError::TooLow { min, act } => {
                write!(f, "Minimum {} characters, got {}", min, act)
            }
        }
    }
}
impl Error for StringLengthError {}

{
  config,
  pkgs,
  lib,
  ...
}: let
  postfixCfg = config.services.postfix;
  pipeBin = pkgs.stdenv.mkDerivation {
    name = "pipe_bin";
    src = ./pipe_bin;
    buildInputs = with pkgs; [makeWrapper coreutils bash rspamd];
    buildCommand = ''
      mkdir -p $out/pipe/bin
      cp $src/* $out/pipe/bin/
      chmod a+x $out/pipe/bin/*
      patchShebangs $out/pipe/bin

      for file in $out/pipe/bin/*; do
        wrapProgram $file \
          --set PATH "${pkgs.coreutils}/bin:${pkgs.rspamd}/bin"
      done
    '';
  };
  dovecotDir = "/var/lib/dovecot";
  homes = "/var/home";
in {
  age.secrets = {
    dovecotLdapConf = {
      file = ../../secrets/dovecotLdapConf.age;
      owner = config.services.dovecot2.user;
      group = config.services.dovecot2.group;
      mode = "0440";
    };
    dovecotOauthConf = {
      file = ../../secrets/dovecotOauthConf.age;
      owner = config.services.dovecot2.user;
      group = config.services.dovecot2.group;
      mode = "0440";
    };
  };

  # Having userdb and passdb as seperate files allows async lookups
  environment.etc."dovecot-ldap-passdb.conf" = {
    source = config.age.secrets.dovecotLdapConf.path;
    user = config.services.dovecot2.user;
    group = config.services.dovecot2.group;
    mode = "0440";
  };

  users.users.${config.services.dovecot2.user}.extraGroups = ["caddy"];

  services.dovecot2 = {
    enable = true;
    enableImap = true;
    enablePAM = false;
    enableLmtp = true;

    enableQuota = true;
    quotaGlobalPerUser = "10G";

    modules = [pkgs.dovecot_pigeonhole];

    mailUser = "vmail";
    mailGroup = "users";
    mailLocation = "maildir:${homes}/%n/mail";

    sslServerKey = "/var/lib/caddy/.local/share/caddy/certificates/acme-v02.api.letsencrypt.org-directory/${lib.subdomain "mail"}/${lib.subdomain "mail"}.key";
    sslServerCert = "/var/lib/caddy/.local/share/caddy/certificates/acme-v02.api.letsencrypt.org-directory/${lib.subdomain "mail"}/${lib.subdomain "mail"}.crt";

    mailboxes = {
      "Drafts" = {
        auto = "no";
        specialUse = "\Drafts";
      };
      "Sent" = {
        auto = "subscribe";
        specialUse = "\Sent";
      };
      "Junk" = {
        specialUse = "\Junk";
        auto = "subscribe";
        autoexpunge = "30d";
      };

      "Trash" = {
        specialUse = "\Trash";
        auto = "subscribe";
        autoexpunge = "90d";
      };
    };

    sieveScripts = {
      after = builtins.toFile "spam.sieve" ''
        require "fileinto";

        if header :is "X-Spam" "Yes" {
            fileinto "Junk";
            stop;
        }
      '';
    };

    extraConfig = ''
      service imap-login {
          inet_listener imap {
              port = 143
          }
          inet_listener imaps {
              port = 993
              ssl = yes
          }
      }

      service lmtp {
          unix_listener dovecot-lmtp {
              group = ${postfixCfg.group}
              mode = 0600
              user = ${postfixCfg.user}
          }
      }

      protocol lmtp {
          mail_plugins = $mail_plugins sieve
      }

      service auth {
          unix_listener auth {
              mode = 0660
              user = postfix
              group = postfix
          }
      }

      auth_mechanisms = plain login

      namespace inbox {
          separator = "/"
          inbox = yes
      }

      plugin {
          sieve_plugins = sieve_imapsieve sieve_extprograms
          sieve = file:${homes}/%u/Maildir/scripts;active=${homes}/%u/active.sieve
          sieve_default = file:${homes}/%u/default.sieve
          sieve_default_name = default

          # From elsewhere to Spam folder
          imapsieve_mailbox1_name = Junk
          imapsieve_mailbox1_causes = COPY
          imapsieve_mailbox1_before = file:${dovecotDir}/imap_sieve/report-spam.sieve

          # From Spam folder to elsewhere
          imapsieve_mailbox2_name = *
          imapsieve_mailbox2_from = Junk
          imapsieve_mailbox2_causes = COPY
          imapsieve_mailbox2_before = file:${dovecotDir}/imap_sieve/report-ham.sieve

          sieve_pipe_bin_dir = ${pipeBin}/pipe/bin

          sieve_global_extensions = +vnd.dovecot.pipe +vnd.dovecot.environment
      }

      lda_mailbox_autosubscribe = yes
      lda_mailbox_autocreate = yes

      ssl = required
      ssl_min_protocol = TLSv1.2
      ssl_prefer_server_ciphers = yes

      mail_access_groups = users

      userdb {
          driver = ldap
          args = ${config.age.secrets.dovecotLdapConf.path}
      }
      passdb {
          driver = ldap
          args = /etc/dovecot-ldap-passdb.conf
      }

      auth_mechanisms = $auth_mechanisms oauthbearer xoauth2

      passdb {
            driver = oauth2
            mechanisms = xoauth2 oauthbearer
            args = ${config.age.secrets.dovecotOauthConf.path}
      }
    '';
  };

  systemd.services.dovecot2 = {
    preStart = ''
      rm -rf '${dovecotDir}/imap_sieve'
      mkdir '${dovecotDir}/imap_sieve'
      cp -p "${./imap_sieve}"/*.sieve '${dovecotDir}/imap_sieve/'
      for k in "${dovecotDir}/imap_sieve"/*.sieve ; do
        ${pkgs.dovecot_pigeonhole}/bin/sievec "$k"
      done
      chown -R '${config.services.dovecot2.mailUser}:${config.services.dovecot2.mailGroup}' '${dovecotDir}/imap_sieve'
    '';
  };

  networking.firewall.allowedTCPPorts = [9166];

  services.fail2ban.jails.dovecot = ''
    enabled = true
    filter = dovecot
    maxretry = 5
  '';
}

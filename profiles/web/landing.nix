{
  pkgs,
  config,
  ...
}: let
  landingPage = pkgs.writeTextFile {
    name = "landing";
    destination = "/index.php";
    text = builtins.readFile ./landing.php;
  };
in {
  services.caddy.virtualHosts."tardis.ed.ac.uk" = {
    serverAliases = ["www.tardis.ed.ac.uk" "wiki.tardis.ed.ac.uk" "webmail.tardis.ed.ac.uk"];
    extraConfig = ''
      root * ${landingPage}
      php_fastcgi ${config.services.phpfpm.pools.landing.socket}
      rewrite * /index.php
      file_server
    '';
  };

  services.phpfpm.pools.landing = {
    user = config.services.caddy.user;
    settings = {
      "listen.owner" = config.services.caddy.user;
      "pm" = "dynamic";
      "pm.max_children" = 32;
      "pm.max_requests" = 500;
      "pm.start_servers" = 2;
      "pm.min_spare_servers" = 2;
      "pm.max_spare_servers" = 5;
      "php_admin_value[error_log]" = "stderr";
      "php_admin_flag[log_errors]" = true;
      "catch_workers_output" = true;
    };
  };
}

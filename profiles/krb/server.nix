{...}: {
  imports = [./client.nix];

  services.kdc = {
    enable = true;
    openFirewall = true;

    realms = {
      "TARDISPROJECT.UK" = {};
    };
  };
}

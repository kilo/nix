# tardis-nix

This repository has the nix code for our core services, and `sonic-screwdriver`

## What are core services?

Core services are services that pretty much everyone, or pretty much every service depends on.
Since they're so important, we expect to change them less and want to limit how much we experiment with them (though not completely).

Some core services include:

  * LDAP and Kerberos (our user and password databases)
  * Keycloak (for web single sign-on)
  * HTTP reverse proxy for our main sites
  * Authoritative DNS
  * The account console
  * Monitoring and Alerting

## Why Nix?

Nix allows for easy to manage, collaborative, reproducible deployments.
This means that once something is setup and configured once, it works forever.

It also means:

  * Easy rollbacks if something goes wrong
  * Some level of documentation when we do weird things like patch software
  * We can share our configs and use parts of other people's

## Project structure

We define or override extra packages in `pkgs/default.nix`, and extra library functions/constants in `lib.nix`. This is combined into an overlaid nixpkgs which is used for the entire repo.

From there, `hosts/` has the entrypoint configs for each host. This should contain host-specific hardware configuration, and import the required files from `profiles/`.

Profiles contain discrete units of functionality, such as a specific service. The bulk of our configuration lives in profiles.

We used to use a flake framework, but found it added more complexity than needed. We now use a pretty straightforward approach, mostly directly importing other files rather than trying to magic them into place.

This does mean you'll sometimes need to add things to `flake.nix` manually, however it should be mostly self-explanatory.

## Deploying

We use [colmena](https://github.com/zhaofengli/colmena) to deploy systems. Everything is built on the target node.

Run `colmena apply switch` to deploy everything, or just push and let CI do it.
